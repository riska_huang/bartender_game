#ifndef PWMRECEIVER_H
#define PWMRECEIVER_H
#include "PinChangeInt.h"

#define START 10
#define THRESHOLD 3
#define SCALE 5
#define OFFSET 5

#define MODE 4
#define MODE2 3

class PWMReceiver{
private:
  //Number's mode setting
  int FreqMode[MODE];
  int GainMode[MODE2];
  float TimeMode[MODE];
  //Vibrator's state
	int VibratorNum;
	int *vibratorFre;
	int *vibratorGain;
  float *vibrateTime;
  bool *dataReady;
  //Receiving value
	int pwm_value = 0;
	int prev_high_time = 0;
	int currentVibrator;
	bool receiving;
	int receiveCount;
public:
	PWMReceiver(int pin);
  void setFreqMode(int *n);
  void setGainMode(int *n);
  void setTimeMode(float *n);
	int getVibratorFreq(int num);
	int getVibratorGain(int num);
  float getVibrateTime(int num);
	bool isDataReady(int num);
	void rise();
	void fall();
};

PWMReceiver::PWMReceiver(int pin) {
	pinMode(pin, INPUT);
  digitalWrite(pin, LOW);
	receiving = false;

	VibratorNum = 2;
	vibratorFre = new int[VibratorNum];
	vibratorGain = new int[VibratorNum];
  vibrateTime = new float[VibratorNum];
  dataReady = new bool[VibratorNum];
	for (int i = 0; i < VibratorNum; i++) {
		vibratorFre[i] = 0;
		vibratorGain[i] = 0;
    vibrateTime[i] = 0;
    dataReady[i] = false;
	}
}


void PWMReceiver::rise() {
	  prev_high_time = millis();
}

void PWMReceiver::fall() {
  int highTime = millis() - prev_high_time;
	if (!receiving && (abs(highTime - START) < THRESHOLD)) {
		receiving = true;
		receiveCount = 0;
	}
	else if(receiving){
		//caculate PWM
		pwm_value = highTime;
		//get the receive value(process the delay)
		int modeValue = pwm_value % SCALE;
		if (modeValue < (SCALE / 2)) {
			pwm_value -= modeValue;
		}
		else {
			pwm_value += SCALE - modeValue;
		}
		receiveCount++;
		switch (receiveCount) {
		case 1:
			currentVibrator = (pwm_value - OFFSET) / SCALE;
			break;
		case 2:
			vibratorFre[currentVibrator] = FreqMode[(pwm_value - OFFSET) / SCALE];
			break;
		case 3:
			vibratorGain[currentVibrator] = GainMode[(pwm_value - OFFSET) / SCALE];
			break;
    case 4:
      vibrateTime[currentVibrator] = TimeMode[(pwm_value - OFFSET)/SCALE];
      dataReady[currentVibrator] = true;
      receiving = false;
      break;
		}
	}
  
}

void PWMReceiver::setFreqMode(int *n){
    for(int i = 0 ; i < MODE; i++)
      FreqMode[i] = n[i];
}

void PWMReceiver::setGainMode(int *n){
   for(int i = 0 ; i < MODE2; i++)
      GainMode[i] = n[i];
}

void PWMReceiver::setTimeMode(float *n){
  for(int i = 0 ; i < MODE; i++)
      TimeMode[i] = n[i];
}

int PWMReceiver::getVibratorFreq(int num) {

	return vibratorFre[num];
}

int PWMReceiver::getVibratorGain(int num) {

	return vibratorGain[num];
}

float PWMReceiver::getVibrateTime(int num) {

  return vibrateTime[num];
}

bool PWMReceiver::isDataReady(int num) {
  bool ready = dataReady[num];
  dataReady[num] = false;
  return ready;
  return vibrateTime[num];
}
#endif


