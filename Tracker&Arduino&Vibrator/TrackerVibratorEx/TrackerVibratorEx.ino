#include "Adafruit_TPA2016.h"
#include "PWMReceiver.h"
#define VibratorNum 2

//Pin Variable
int buttonPin = 2; //if you want to change your pin , just do here
int rightVibPin = 3; //if you want to change your pin , just do here
int leftVibPin = 4; //if you want to change your pin , just do here
int trackerPin = 5; //if you want to change your pin , just do here

//VibratorMode
int FreqMode[4] = {25, 50, 125, 250};
int GainMode[3] = {0, 28, 58};
float TimeMode[4] = {0.1, 0.5, 1.0, 2.0};

//[PWMReceiver]
PWMReceiver pwmReceiver(trackerPin);
int freq[VibratorNum], gain[VibratorNum];
float vibrateTime[VibratorNum];
uint8_t latest_interrupted_pin = trackerPin;

//[Vibrator]
//Timer interrupt
int timer = 0; // for calculate frequence
//audioamp
Adafruit_TPA2016 audioamp = Adafruit_TPA2016();
int vibWaveLength[2], vibGain[2];
//For fadein fadeout effect
float timeStage[2][4], waveLenStage[2][5], gainStage[2][5];


void setup() {
 
  Serial.begin(9600);
  
  //Initial the vibrator
  vibratorInitial();
  //Inital the receiver
  pwmReceiverInitial();

}
// [10 Hz Timer Interrupt]
ISR(TIMER1_COMPA_vect){
 
  if(vibrateTime[0] > 0.0f)
    vibrateTime[0] = vibrateTime[0] - 0.01f;

  if(vibrateTime[1] > 0.0f)
    vibrateTime[1] = vibrateTime[1] - 0.01f;
  
  //[Vibrator Fadein Fadeout Part]
  for(int i = 0 ; i < 2 ; i++){
    if(vibrateTime[i] > 0){
        //Check which the time stage it is currently
        //Assign the waveLength and gain
        if(vibrateTime[i] >= timeStage[i][0]){
          vibWaveLength[i] = waveLenStage[i][0];
          vibGain[i] = gainStage[i][0];
        }
        else if((vibrateTime[i] < timeStage[i][0]) && ((vibrateTime[i] >= timeStage[i][1]))){
          vibWaveLength[i] = waveLenStage[i][1];
          vibGain[i] = gainStage[i][1];
        }
        else if((vibrateTime[i] < timeStage[i][1]) && ((vibrateTime[i] >= timeStage[i][2]))){
          vibWaveLength[i] = waveLenStage[i][2];
          vibGain[i] = gainStage[i][2];
        }
        else if((vibrateTime[i] < timeStage[i][2]) && ((vibrateTime[i] >= timeStage[i][3]))){
          vibWaveLength[i] = waveLenStage[i][3];
          vibGain[i] = gainStage[i][3];
        }
        else if(vibrateTime[i] < timeStage[i][3]){
          vibWaveLength[i] = waveLenStage[i][4];
          vibGain[i] = gainStage[i][4];
        }
    }
  }
}

//[1k Hz Timer Interrupt]
ISR(TIMER2_COMPA_vect){
  timer++;
  if(timer==30000)
    timer = 0;
}

//[PWMReceiver]
//Rising Voltage Interrupt for the PWMReceiver
void rising()
{
  pwmReceiver.rise();
  latest_interrupted_pin=PCintPort::arduinoPin;
  PCintPort::attachInterrupt(latest_interrupted_pin, &falling, FALLING);
}

//[PWMReceiver]
//Falling Voltage Interrupt for the PWMReceiver
void falling() {
  pwmReceiver.fall();
  latest_interrupted_pin=PCintPort::arduinoPin;
  PCintPort::attachInterrupt(latest_interrupted_pin, &rising, RISING);
}
 
void loop() {
    
    //Check is there any new value
    if(pwmReceiver.isDataReady(0)){
      //Get the num0 freq, gain and time
      freq[0] = pwmReceiver.getVibratorFreq(0);
      gain[0] = pwmReceiver.getVibratorGain(0);
      vibrateTime[0] = pwmReceiver.getVibrateTime(0);
      if(freq[0] == 0){
         //Vibrator Stop
         stopVibration(0);
      }
      else{
        vibrate(0, freq[0], gain[0], vibrateTime[0]);
      }
    }

    //Check is there any new value
    if(pwmReceiver.isDataReady(1)){
      //Get the num1 freq and gain
      freq[1] = pwmReceiver.getVibratorFreq(1);
      gain[1] = pwmReceiver.getVibratorGain(1);
      vibrateTime[1] = pwmReceiver.getVibrateTime(1);
      if(freq[1] == 0){
          //Vibrator Stop
          stopVibration(1);
      }
      else{ 
         vibrate(1, freq[1], gain[1], vibrateTime[1]);
      }
    }

    //[Update
    updateVibration();
}

void print(int num, int fre, int gain, float t){
  Serial.print("Vibrator: ");
  Serial.print(num);
  Serial.print(" Freq: ");
  Serial.print(fre);
  Serial.print(" Gain: ");
  Serial.print(gain);
  Serial.print(" Time: ");
  Serial.println(t);
}

void vibratorInitial(){
  pinMode(rightVibPin, OUTPUT);
  pinMode(leftVibPin, OUTPUT);
  timerInitial();
  audioampInitial();
}

void pwmReceiverInitial(){
  
  //[PWMReceiver]
  //Interrupt for the PWMReceiver
  PCintPort::attachInterrupt(trackerPin, &rising, RISING);

  //Setup the Frequency, Gain, Timer Mode
  pwmReceiver.setFreqMode(FreqMode);
  pwmReceiver.setGainMode(GainMode);
  pwmReceiver.setTimeMode(TimeMode);
  
}

void timerInitial() {
  cli();//stop interrupts
    //set timer1 interrupt at 100Hz 
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 100hz increments
    OCR1A = 624;// = (16*10^6) / (10*256) - 1 (must be <65536)
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS11 bits for 256 prescaler
    TCCR1B |= (1<<CS12);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);

    //timer2
    TCCR2A = 0;// set entire TCCR2A register to 0
    TCCR2B = 0;// same for TCCR2B
    TCNT2  = 0;//initialize counter value to 0
    // set compare match register for 1khz increments
    OCR2A = 249;// = (16*10^6) / (1000*64) - 1 (must be <256)
    // turn on CTC mode
    TCCR2A |= (1 << WGM21);
    // Set CS21 bit for 64 prescaler
    TCCR2B |= (1 << CS22);   
    // enable timer compare interrupt
    TIMSK2 |= (1 << OCIE2A);

  sei();//allow interrupts
}
void audioampInitial(){
  audioamp.begin();
  audioamp.enableChannel(true, true);
  audioamp.setLimitLevelOff();//dont change 
  audioamp.setAGCCompression(TPA2016_AGC_OFF);//dont change
}

//do vibration
void vibrate(int num, int frequence, int gain, float vibTime){//half_wavelength(ms)
  
  //Assign the time Stage
  float max1 = 0.1, max2 = 0.2;
  timeStage[num][0] = vibTime - (vibTime * 0.1 < max1 ? vibTime * 0.1 : max1);
  timeStage[num][1] = vibTime - (vibTime * 0.2 < max2 ? vibTime * 0.2 : max2);
  timeStage[num][2] = (vibTime * 0.2 < max2 ? vibTime * 0.2 : max2);
  timeStage[num][3] = (vibTime * 0.1 < max1 ? vibTime * 0.1 : max1);
  Serial.println(vibTime);
  for(int i =0; i<4; i++){
    Serial.println(timeStage[num][i]);
  }
  //Assign the waveLen Stage
  waveLenStage[num][0] = 500/frequence * 3;
  waveLenStage[num][1] = 500/frequence * 3 / 2;
  waveLenStage[num][2] = 500/frequence;  
  waveLenStage[num][3] = 500/frequence * 3 / 2;
  waveLenStage[num][4] = 500/frequence * 3;
  
  //Assign the Gain Stage
  gainStage[num][0] = gain / 3;
  gainStage[num][1] = gain / 3 * 2;
  gainStage[num][2] = gain;
  gainStage[num][3] = gain / 3 * 2;
  gainStage[num][4] = gain / 3;

  vibWaveLength[num] = waveLenStage[num][0];
  vibGain[num] = gainStage[num][0];

  audioamp.enableChannel(true, true);
}

void updateVibration(){
  
    int pinNum[2];
    pinNum[0] = rightVibPin; pinNum[1] = leftVibPin;
    
    for(int i = 0 ; i < 2 ; i++){
      if(vibrateTime[i] > 0){
        audioamp.setGain(vibGain[i]-28);
        if(int(floor(timer/vibWaveLength[i]))%2==1)
          digitalWrite(pinNum[i],HIGH);
        else 
          digitalWrite(pinNum[i],LOW);
      }
    }
}

void stopVibration(int num){

  if(num == 0){
    audioamp.enableChannel(false, true);
  }
  else if(num == 1){
    audioamp.enableChannel(true, false);
  }
  
}

