﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject shaker;
    public Recipe recipe;
	// The drink that appears after order complete
	public Transform drinkPosition;
	public GameObject margarita;
	public GameObject screwDriver;

	//----------------UI----------------//
	public Text accumulatedDistance;
    public Text timeOfShake;
    public Text recipeNumber;
    public Text endText;
    //----------------------------------//

    private Vector3 referencePoint;

    //------THRESHOLD IN EACH AXIS------//
    public float xThresh;
	public float yThresh;
	public float zThresh;
    //----------------------------------//

    //----------VARIABLES USED----------//

    // Current distance accumulated
    private float currentDistance;
    // All orders
    private List<int> orders;
    // The number of order currently handling
    private int orderNumber;
    // The number of instruction in current recipe
    private int numberOfInstruction;
    // The time of shake the user has done
	private int shakeCount;
    // Boolean indicating whether this order has been done
    private bool state;
	// Cocktails
	private GameObject cocktail;

    //----------------------------------//


	public bool Send = true;
	//Num: 0,1 (for two vibrator)
	public int num = 1;
	//FreqMode: 0~3
	public int freqMode = 0;
	//GainMode: 0~2
	public int gainMode = 0;
	//TimeMode: 0~3
	public float timeMode = 0;


    // Use this for initialization
    void Start () {

        //-------IMPORT RECIPE LIBRARY------//
        recipe = recipe.GetComponent<Recipe>();
        //----------------------------------//

        //-------RESET ALL PARAMETERS-------//
        shakeCount = 0;
		currentDistance = 0;
        numberOfInstruction = 0;
        orderNumber = 0;
        state = false;
        //----------------------------------//

		//----------RESET ALL TEXT----------//
		endText.text = ("");
		accumulatedDistance.text = ("");
		timeOfShake.text = ("");
		recipeNumber.text = ("");
		recipe.recipeName.text = ("");
		recipe.ingredient.text = ("");
        recipe.dirImage.texture = null;
        recipe.numOfShake.text = ("");
		//----------------------------------//

        //-------OBTAIN INITIAL POINT-------//
        referencePoint = shaker.transform.position;
		//----------------------------------//

		orders = new List<int>();
		orders.Add(1);
		orders.Add(0);

	}
	
	// Update is called once per frame
	void Update () {

		//-------------UI UPDATE------------//
		UIUpdate();
		if (Send)
		{
			//Start the vibrator according to the setting
			GetComponent<TrackerOutput>().startVibrate(num, freqMode, gainMode, timeMode);
			Send = true;
		}
		//----------------------------------//

		Debug.Log ("velocity" + shaker.GetComponent<Rigidbody> ().velocity);
		OrderHandling(orders.Count);       
	}

	// Function for UI update
	void UIUpdate()
	{
		accumulatedDistance.text = "Distance = " + currentDistance.ToString();
		timeOfShake.text = ("The ") + (shakeCount + 1).ToString();
		switch (shakeCount)
		{
			case 0:
				timeOfShake.text += ("st shake");
				break;
			case 1:
				timeOfShake.text += ("nd shake");
				break;
			case 2:
				timeOfShake.text += ("rd shake");
				break;
			default:
				timeOfShake.text += ("th shake");
				break;
		}
		recipeNumber.text = ("The ") + (orderNumber + 1).ToString();
		switch (orderNumber)
		{
			case 0:
				recipeNumber.text += ("st order");
				break;
			case 1:
				recipeNumber.text += ("nd order");
				break;
			case 2:
				recipeNumber.text += ("rd order");
				break;
			default:
				recipeNumber.text += ("th order");
				break;
		}
	}

	void drinkDisplay(int drinkNumber)
	{
		Debug.Log("Hi");
		switch (drinkNumber)
		{
			case 0:
				cocktail = Instantiate(margarita, drinkPosition.position, drinkPosition.rotation);
				break; 
			case 1:
				cocktail = Instantiate(screwDriver, drinkPosition.position, drinkPosition.rotation);
				break;
			default:
				break;
		}
	}

	// Handles all orders
	void OrderHandling(int totalNumber)
	{
		// If there is still order left
        if (orderNumber < totalNumber)
        {
            // If current order hasn't been finished
            if (!state)
            {
                state = ShakeTheDrink(recipe.recipeBook(orders[orderNumber], numberOfInstruction));
            }
            // Finished, move on to next order and reset all parameters
            else
            {
				// Destroy previous cocktail
				if((orderNumber > 0) && (orderNumber < totalNumber))
				{
					Destroy(cocktail);
				}
				drinkDisplay(orders[orderNumber]);
				state = false;
                shakeCount = 0;
                currentDistance = 0;
                numberOfInstruction = 0;
                orderNumber ++;
            }
        }

        // All orders are done
        else
        {
            accumulatedDistance.text = ("");
            timeOfShake.text = ("");
			recipeNumber.text = ("");
			recipe.recipeName.text = ("");
			recipe.ingredient.text = ("");
			endText.text = ("Orders Completed!");	
        }
	}

    // Handles the recipe instructions one by one
    bool ShakeTheDrink(Vector2[] instruction)
    {
        // If still recipe left, then move onto next recipe   
        if (numberOfInstruction < instruction.Length)
        {
            // If current time of shake hasn't reach the current instructrion
            if (shakeCount < (int)instruction[numberOfInstruction].y)
            {
                // Keep shaking
                shakeInAxis((int)instruction[numberOfInstruction].x);
            }
            // Time of shake reached the value
            else
            {
                // Reset time of shake and move onto next instruction
                shakeCount = 0;
                numberOfInstruction += 1;
            }
            return false;
        }
        // Returns that all recipes are done
        else
        {
            return true;
        }
    }

    // Calculates the time of shake in each axis
    void shakeInAxis(int axis)
    {
        float displacement = 0;
        displacement = distanceCalculate(axis);
        currentDistance += displacement;
        switch (axis)
        {
            // Shake in x-axis
            case 1:
                // If threshold is reached, then we calculate it as a complete shake
                if (currentDistance >= xThresh)
                {
                    shakeCount += 1;
                    currentDistance = 0;
                }
                break;
            // Shake in y-axis
            case 2:
                if (currentDistance >= yThresh)
                {
                    shakeCount += 1;
                    currentDistance = 0;
                }
                break;
            // Shake in z-axis
            case 3:
                if (currentDistance >= zThresh)
                {
                    shakeCount += 1;
                    currentDistance = 0;
                }
                break;
        }
        // Update the reference point
        referencePoint = shaker.transform.position;
        return;
    }

    // Calculates the displacement
    float distanceCalculate(int axis)
    {
        float distance = 0;
        switch (axis)
        {
            // Displacement in the x axis
            case 1:
                distance = Mathf.Abs(shaker.transform.position.x - referencePoint.x);
                break;
            // Displacement in the y axis
            case 2:
                distance = Mathf.Abs(shaker.transform.position.y - referencePoint.y);
                break;
            // Displacement in the z axis
            case 3:
                distance = Mathf.Abs(shaker.transform.position.z - referencePoint.z);
                break;
            default:
                return 0;
        }
        // Threshold in distance is put to handle the calibration problem
        if ((distance >= 0.01) && (distance < 0.1))
        {
            return distance;
        }
        else
        {
            return 0;
        }
    }           
}
