﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

//This is the recipe library
public class Recipe : MonoBehaviour {

	// Text from the recipe book
	//public Text recipeName;
	//public Text ingredient;

	public string drinkName;
    Vector2[] recipe;

    //UI initialisation
    //public Text recipeNumber;
    public Text recipeName;
    public Text ingredient;
    //public Text timeOfShake;
    public RawImage dirImage;
    public Text numOfShake;

    //arrow texture initialisation
    public Texture arrowX;
    public Texture arrowY;
    public Texture arrowZ;

	// The function that handles all orders 
	public Vector2[] recipeBook(int order, int numberOfInstruction)
    {
        //Vector2[] recipe;
        switch (order)
        {
            case 0:
                recipe = margarita(numberOfInstruction);
				drinkName = "margaritaPrefab";
                //recipeNumber.text = ("Recipe No: " + (order + 1));
                recipeName.text = "Margarita";

				break;
            case 1:
                recipe = screwDriver(numberOfInstruction);
                //recipeNumber.text = ("Recipe No: " + (order+1));
                recipeName.text = "Screwdriver";
                break;
            default:
                recipe = new Vector2[]{ new Vector2 (0, 0)};
                break;
        }
        return recipe;
    }

    //Margarita cocktail
    Vector2[] margarita(int numberOfInstruction)
    {
        //Vector2[] recipe;
        recipe = new Vector2[] {
               //Taquilla 35ml
               new Vector2 (1, 7),
               //Cointreau 20ml
               new Vector2 (2, 4),
               //Lemon Juice 15ml
               new Vector2 (3, 3),
               //A bit of salt
               new Vector2 (2, 1)
        };
        // Print out recipe name
        //recipeName.text = ("Margarita");
        RecipeInstruction();
        // Print the current ingredient
        switch (numberOfInstruction)
		{
			case 0:
				ingredient.text = ("Taquilla 35ml");
				break;
			case 1:
				ingredient.text = ("Cointreau 20ml");
				break;
			case 2:
				ingredient.text = ("Lemon Juice 15ml");
				break;
			case 3:
				ingredient.text = ("A bit of salt");
				break;
			default:
				ingredient.text = ("");
				break;
		}

        return recipe;
    }

    //Screwdriver cocktail
    Vector2[] screwDriver(int numberOfInstruction)
    {
        //Vector2[] recipe;
        recipe = new Vector2[] {
               //Vodka 50ml
               new Vector2 (3, 2),
               //Orange Juice 100ml
               new Vector2 (1, 4)
        };
        
        // Print out recipe name
        //recipeName.text = ("Screwdriver");
        RecipeInstruction();
        // Print the current ingredient
        switch (numberOfInstruction)
		{
			case 0:
				ingredient.text = ("Vodka 50ml");
				break;
			case 1:
				ingredient.text = ("Orange Juice 100ml");
				break;
			default:
				ingredient.text = ("");
				break;
		}
		return recipe;
    }

    
    void RecipeInstruction()
    {
        foreach (Vector2 recipes in recipe.Reverse())
        {
            int dirRecipe = (int)recipes.x;
            switch (dirRecipe)
            {
                case 1:
                    dirImage.texture = arrowX;
                    break;
                case 2:
                    dirImage.texture = arrowY;
                    break;
                case 3:
                    dirImage.texture = arrowZ;
                    break;
                default:
                    break;
            }

            int numInst = (int)recipes.y;
            numOfShake.text = ("x " + numInst);
        }
    }
    
}
