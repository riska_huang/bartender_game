﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTrackerSender : MonoBehaviour
{
	public bool Send = true;
	//Num: 0,1 (for two vibrator)
	public int num = 1;
	//FreqMode: 0~3
	public int freqMode = 0;
	//GainMode: 0~2
	public int gainMode = 0;
	//TimeMode: 0~3
	public float timeMode = 0;

	// Update is called once per frame
	void Update()
	{
		if (Send)
		{
			//Start the vibrator according to the setting
            GetComponent<TrackerOutput>().startVibrate(num, freqMode, gainMode, timeMode);
			Send = true;
		}
	}
}
