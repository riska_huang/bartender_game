﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackerOutput : MonoBehaviour {

	public SteamVR_TrackedObject tranckedObject;
	private SteamVR_Controller.Device device;

	private List<int> outPut = new List<int>();

	private const int START = 10;
	private const int BLANK = 2;
	private const int SCALE = 5;
	private const int OFFSET = 5;

	public void startVibrate(int num, int freqMode, int gainMode, float timeMode)
	{


		device = SteamVR_Controller.Input((int)tranckedObject.index);
		//Start signal
		outPut.Clear();
		//Send Pulse: Caculate the HIGH time according to the content
		//We send the number(which vibrator), the frequency, finally the gain
		outPut.Add(num * SCALE + OFFSET);
		outPut.Add(freqMode * SCALE + OFFSET);
		outPut.Add(gainMode * SCALE + OFFSET);
		outPut.Add((int)(timeMode * SCALE) + OFFSET);
		
		//Send the Start Signal
		device.TriggerHapticPulse(START);

		StartCoroutine(SendPulse((BLANK+START)/1000.0f));
	}

	IEnumerator SendPulse(float time)
	{
		yield return new WaitForSeconds(time);
		Debug.Log (outPut.Count);
		if(outPut.Count > 0)
		{
			ushort t = (ushort)outPut[0];
			outPut.RemoveAt(0);
			device.TriggerHapticPulse(t);
			StartCoroutine(SendPulse((BLANK+t) / 1000.0f));
		}
	}
}
